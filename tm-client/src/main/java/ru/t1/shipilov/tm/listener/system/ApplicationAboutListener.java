package ru.t1.shipilov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.shipilov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.shipilov.tm.event.ConsoleEvent;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String DESCRIPTION = "Show developer info.";

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = systemEndpoint.getAbout(request);

        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + response.getApplicationName());
        System.out.println();

        System.out.println("[ABOUT]");
        System.out.println("AUTHOR: " + response.getAuthorName());
        System.out.println("E-MAIL: " + response.getAuthorEmail());
        System.out.println();
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
