package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLog();

    @NotNull
    String getApplicationName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAdminLogin();

    @NotNull
    String getAdminPassword();

}
